//
//  ViewController.swift
//  JyotiGuptaTest
//
//  Created by Sorabh Gupta on 12/1/17.
//  Copyright © 2017 Jyoti Gupta. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblViewData: UITableView!
    var sectionTitle = NSMutableArray()
    var rowTitle = NSMutableArray()
    
    
     func viewDidLoad()
    {
        super.viewDidLoad()
        
        let arrayList = ["Rahul","Rishav","Honey","Sagar","Jyoti","Subhi","Ankit","Sikhar","Yudhveer","Arpit","Bhanu","Narindar","Beena","Nisha"]

        // We can use letters array for sections:
        
        var letters: [Character] = []
        letters = arrayList.map { (name) -> Character in
            return name[name.startIndex]
        }
        
        letters = letters.sorted()
        letters = letters.reduce([], { (list, name) -> [Character] in
            if !list.contains(name) {
                return list + [name]
            }
            return list
        })
        
        print("Letters =",letters)
        
        // We can use contacts array for section and rows:
        
        var contacts = [Character: [String]]()
        
        for entry in arrayList
        {
            if contacts[entry[entry.startIndex]] == nil
            {
                contacts[entry[entry.startIndex]] = [String]()
            }
            contacts[entry[entry.startIndex]]!.append(entry)
        }
        
         print("Contacts =",contacts)
        
        // We can also use arrayOfArrays for rows:
        
        let unicodeScalarA = UnicodeScalar("a")!
        var arrayOfArrays = arrayList.reduce([[String]]())
        { (output, value) -> [[String]] in
            var output = output
            
            if output.count < 26 {
                output = (1..<27).map{ _ in return []}
            }
            
            if let first = value.characters.first {
                let prefix = String(describing: first).lowercased()
                let prefixIndex = Int(UnicodeScalar(prefix)!.value - unicodeScalarA.value)
                var array = output[prefixIndex]
                array.append(value)
                output[prefixIndex] = array.sorted()
            }
            return output
        }
        arrayOfArrays = arrayOfArrays.filter { $0.count > 0}
        print("arrayOfArrays =",arrayOfArrays)
    }

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return sectionTitle.count;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       // var arrayRow = contacts.section
        
        return rowTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : UITableViewCell = tblViewData.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = "Rahul"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 40
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return sectionTitle[section] as? String
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

